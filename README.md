Gradient scatter and normal plot in MATLAB
======================================================================

This MATLAB package makes it easy to plot gradient color changing in the 
direction of y-axis, something similar to probabilistic plot with 
confidence interval. It is most useful for time series plot where a 
distinguish is needed to be made using different colors. 

The package allows you to use either point or area plots at the same time. 
The gradient color can be customized, it can be continuous or discrete, and
the number of colors can also be specified in discrete mode. 

It is also possible to paint a gradient color on the y-axis direction based
on the global or local maximum and minimum values. In local mode, color 
intensity is calculated based on the maximum and minimum values of a time 
instance. 

The median, mean, or any customized set of numbers can be used as the 
center point for confidence interval type of plots. 



Installation
------------------------
Similar to other MATLAB packages, all you need is to exxtract the zip folder
and add the path in the MATLAB path using "Set Path" from the "HOME" ribbon.



Addendum
--------

### Requirements

Please note that Schemer requires MATLAB.


### Further information

For details on how the method was implemented, see
[this Undocumented Matlab article](http://undocumentedmatlab.com/blog/changing-system-preferences-programmatically).


  [matlab-schemes]: https://github.com/scottclowe/matlab-schemes
  [fex]:            http://mathworks.com/matlabcentral/fileexchange/53862-matlab-schemer
  [flattr]:         https://flattr.com/submit/auto?user_id=scottclowe&url=https://github.com/scottclowe/matlab-schemer&title=MATLAB-schemer&tags=github&category=software
  [gratipay]:       https://gratipay.com/matlab-schemer/
  [bountysource]:   https://www.bountysource.com/teams/matlab-schemer
