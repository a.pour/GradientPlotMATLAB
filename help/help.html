<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gradient Plot in MATLAB</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <style>
        .container-fluid {
          padding: 40px 60px;
          width: 100%;
        }
        .code-inline{
          background: lightblue;
          border-radius: 5px;
          font-family: 'Courier New', Courier, monospace;
          padding: 1px 5px;
        }
        .linenums li {
          list-style-type: decimal !important;
        }
        pre.prettyprint {
          border: none !important;
        }
        .btn {
          font-family: 'Courier New', Courier, monospace;
          cursor: pointer;
        }
    </style>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=matlab&amp;skin=default"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!--<script type="text/javascript" src="syntaxhighlighter.js"></script>-->
</head>
<body>
  <div class="container-fluid bg-light">
    <div class="jumbotron" style="background-color:white;">
      <h2 class="display-4">Gradient colour plot in MATLAB</h2>
      <p class="lead">This package allows users to plot time series with gradient colour of interest on the y-axis direction. 
      The function is very intuitive to use, and the final plot can be modified with common MATLAB commands, e.g., xlabel, title, subplot, etc. 
      Users can create scatter or line plot based on their requirements. Also, it is possible to define a global or local maximum/minimum for colour intensity calculations. 
      This tutorial tried to depict different capabilities of the package through multiple example. 
      </p>
      <p class="lead">The package will be maintained at <a href="https://gitlab.com/a.pour/GradientPlotMATLAB.git" target="_blank">gitlab repository</a>.</p>
      <p class="lead">If you have any question, drop me an inquery at <a href="http://alipourmousavi.com/contact.html" target="_blank">my personal website</a> with <em>gradient plot in MATLAB</em> in the subject line.</p>
      <p class="font-italic">Seyyed Ali Pourmousavi Kani, 2018</p>
      <hr class="my-4">

      <p>The idea developed in this package is to create a figure with different colour with respect to the y-axis values of a set of given time series.
        While it is possible to change colour in <span class="code-inline">scatter</span> plot in MATLAB, a capability does not exist to have gradient colour for different clusters of data. It will be explained later through several examples. Plotting confidence interval is an example of such figures, as shown in Figure 1. It is worth mentioning that Figure 1 is a plot of confidence interval for probabilistic prediction of wind generation.
      </p>

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="example.PNG" class="figure-img img-fluid rounded" alt="Figure 1. An example of a gradient plot in R">
          <figcaption class="figure-caption text-center">Figure 1. An example of a gradient plot in R - <a href="http://ieeexplore.ieee.org/document/5275816/" target="_blank">REFERENCE</a></figcaption>
        </figure>
      </div>

      <p>
        Let's assume that we have hourly data of a physical phenomenon for 10000 days. The hypothetical data comes with this package in <code>example</code> folder, which is called <code>DATA.mat</code>. To plot a figure of the data in which every hour of every day is presented in a different colour based on the global minimum and maximum values of the entire dataset using <span class="code-inline">gradientplot()</span>, we can have the following code:
      </p>

      <pre class="prettyprint linenums">      if ~exist('DATA' , 'var')
          load('example\DATA.mat');
      end
      options = struct('colorstyle' , 'continuous' , ...
                       'intensestyle' , 'global' , ...
                       'linestyle' , '-' , ...
                       'midpoint' , 'none');
                   
      gradientplot(1:24 , DATA(randi(length(DATA) , length(DATA)*0.2 , 1) , :) , options);</pre>

      <p>
        In the first three lines, <span class="code-inline">DATA</span> is loaded if it is not in the workspace. Then, <span class="code-inline">options</span> are set to appropriate values. To plot, I used only 20% of the existing data. You can see the final plot in Figure 2. 
      </p>

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="global1.PNG" class="figure-img img-fluid rounded" alt="Figure 2. Global colour intensity on y-axis direction">
          <figcaption class="figure-caption text-center">Figure 2. Global colour intensity on y-axis direction</figcaption>
        </figure>
      </div>

      <p>
        In Figure 2, no colormap is specified and the default colormap, i.e., <span class="code-inline">spring</span>, is used. To specify a colormap, you can use the existing colormaps in MATLAB, or create one for yourself and set it in <span class="code-inline">options</span>, as follows:
      </p>

      <pre class="prettyprint linenums">         map = [0, 0, 0.3 ; ...
          0, 0, 0.4 ; ...
          0, 0, 0.5 ; ...
          0, 0, 0.6 ; ...
          0, 0, 0.8 ; ...
          0, 0, 1.0];
      
          options.colormap = map;</pre>
      
      The results look like the graph in Figure 3:

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="global2.PNG" class="figure-img img-fluid rounded" alt="Figure 3. Applying a customised colormap">
          <figcaption class="figure-caption text-center">Figure 3. Applying a customised colormap</figcaption>
        </figure>
      </div>

      It is also possible to have a discrete, instead of continuous, gradient colour by specifyinig the number of colours:

      <pre class="prettyprint linenums">         options = struct('colorstyle' , 'discrete' , ...
                 'colormap' , 'autumn' , ...
                 'colorstepno' , 6 , ...
                 'intensestyle' , 'global' , ...
                 'linestyle' , '-' , ...
                 'midpoint' , 'none');</pre>
      
      Where 6 colours are used to paint the lines in Figure 4. 

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="global3.PNG" class="figure-img img-fluid rounded" alt="Figure 4. Descrete colours instead of continous option with 6 steps">
          <figcaption class="figure-caption text-center">Figure 4. Descrete colours instead of continous one with 6 steps</figcaption>
        </figure>
      </div>

      Instead of line plot, We can have scatter plot. To do so, we have to change the <span class="code-inline">'linestyle'</span>, while other options stay the same as the previous one:

      <pre class="prettyprint linenums">         options.linestyle = '.';</pre>

      The result looks like the graph in Figure 5.

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="global4.PNG" class="figure-img img-fluid rounded" alt="Figure 5. Scatter plot instead of line plot">
          <figcaption class="figure-caption text-center">Figure 5. Scatter plot instead of line plot</figcaption>
        </figure>
      </div>

      We can use <span class="code-inline">jitter</span> and <span class="code-inline">jitteramount</span> to make the figure more beautiful, as follows:

      <pre class="prettyprint linenums">         options = struct('colorstyle' , 'discrete' , ...
          'colormap' , 'winter' , ...
          'colorstepno' , 6 , ...
          'intensestyle' , 'global' , ...
          'linestyle' , '.' , ...
          'midpoint' , 'none' , ...
          'jitter' , true , ...
          'jitteramount' , 0.4 , ...
          'filledmarker' , true);</pre>

      And the results are shown in Figure 6.

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="local0.PNG" class="figure-img img-fluid rounded" alt="Figure 6. Scatter plot with jitter settings">
          <figcaption class="figure-caption text-center">Figure 6. Scatter plot with jitter settings</figcaption>
        </figure>
      </div>

      The <span class="code-inline">gradientplot</span> allows user to apply colour intensity based on the local minimum and maximum values. In other words, colour intensity will be calculated based on the max/min values of its own cluster (or time stamp) instead of the global one. To do so, we have to change <span class="code-inline">intensestyle</span> from <span class="code-inline">'global'</span> to <span class="code-inline">local</span>:

      <pre class="prettyprint linenums">         options.intensestyle = 'local';</pre>

      The results (with the other setting similar to the previous ones) is shown in Figure 7. It is worth mentioning that the same functionality exist for <span class="code-inline">'continuous</span> colour setting and line plot, i.e., <span class="code-inline">'-'</span>.

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="local1.PNG" class="figure-img img-fluid rounded" alt="Figure 7. Scatter plot with jitter settings and local max/min operation">
          <figcaption class="figure-caption text-center">Figure 7. Scatter plot with jitter settings and local max/min operation</figcaption>
        </figure>
      </div>

      Sometimes, it is needed to have a color reflection, e.g., in confidence interval plots. To do so, it is as easy as setting <span class="code-inline">'midpoint'</span> in the <span class="code-inline">options</span>. There are several way to do this: 1) to specify <span class="code-inline">'mean'</span> of every time stamp as the middle point, 2) to specify <span class="code-inline">'median'</span> of every time step as the middle point, 3) to specify a vector of same length of time stamp of each series by setting <span class="code-inline">'midpoint'</span> to <span class="code-inline">'custom'</span> and specifying a vector in <span class="code-inline">custommidval</span>.

      <pre class="prettyprint linenums">         options = struct('colorstyle' , 'discrete' , ...
          'colormap' , 'winter' , ...
          'colorstepno' , 6 , ...
          'intensestyle' , 'local' , ...
          'linestyle' , '.' , ...
          'midpoint' , 'mean' , ...
          'jitter' , true , ...
          'jitteramount' , 0.4, ...
          'filledmarker' , true);</pre>

      <div class="clearfix">
        <figure class="figure col-8 offset-2 clearfix">
          <img src="local2.PNG" class="figure-img img-fluid rounded" alt="Figure 8. Local scatter plot with jitter settings and color reflection">
          <figcaption class="figure-caption text-center">Figure 8. Local scatter plot with jitter settings and color reflection</figcaption>
        </figure>
      </div>
      The figure with the same settings, but with <span class="code-inline">'-'</span> and <span class="code-inline">'continuous</span> colour format in Figure 9:

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="local3.PNG" class="figure-img img-fluid rounded" alt="Figure 9. Local line plot with color reflection using mean value">
          <figcaption class="figure-caption text-center">Figure 9. Local line plot with color reflection using mean value</figcaption>
        </figure>
      </div>  

      A colormap can be show in the right side of the figure with specific label and font size, as follows:

      <pre class="prettyprint linenums">         options.showcolorbar = true;
          options.colorbarlabel = 'test colorbar - winter';
          options.colorbarfontsize = 10;</pre>

      <div class="clearfix">
        <figure class="figure col-8 offset-2">
          <img src="local4.PNG" class="figure-img img-fluid rounded" alt="Figure 10. colorbar and its options">
          <figcaption class="figure-caption text-center">Figure 10. colorbar and its options</figcaption>
        </figure>
      </div>

      If it is needed, the colour intensity matrix can be exported as an output:

      <pre class="prettyprint linenums">         ColourIntensity = gradientplot(1:24 , DATA(randi(length(DATA) , length(DATA)*0.2 , 1) , :) , options);</pre>
      </div>

      <!-- ACCORDION -->
      <h3>Settings:</h3>
      <div id="accordion">
        <div class="card">
          <div class="card-header" id="headingOne">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  <b>XDATA</b>
                  TYPE: vector | SIZE: N
              </button>
            </h5>
          </div>
      
          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                This data represents the x-axis values for the plot. The number of element, N, should be equal to the number of columnc in YDATA.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <b>YDATA</b>
                TYPE: matrix | SIZE: M-by-N
              </button>
            </h5>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                This is the actual M time series with length of N. The color intensity will be calculated based on this input and other specifications.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingThree">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <b>OPTIONS</b>
                TYPE: struct | SIZE: N/A
              </button>
            </h5>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
                This is the general structure containing all the settings that are allowed. Different settings and their applications are described below.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingFour">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                <b>'colormap'</b>
                TYPE: char or numeric | OPTIONS: check out below
              </button>
            </h5>
          </div>
          <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
            <div class="card-body">
                To select a colormap from exisiting list of MATLAB, or specify a different one, DEFAULT = 'spring'. The available list of colormap in MATLAB is 'parula' , 'jet' , 'hsv' , 'hot' , 'cool' , 'spring' , 'summer' , 'autumn' , 'winter' , 'gray' , 'bone' , 'copper' , 'pink' , 'lines' , 'colorcube' , 'prism' , 'flag' , and 'white'.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingFive">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                <b>'colorstyle'</b>
                TYPE: char | OPTOINS: 'continous' , 'discrete' 
              </button>
            </h5>
          </div>
          <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
            <div class="card-body">
                For continuous gradient colors, select 'continuous'; for a scatter/plot with specific number of colors, select 'discrete', DEFAULT='continuous'
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingSix">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                <b>'colorstepno'</b>
                TYPE: numeric | OPTIONS: any positive integer
              </button>
            </h5>
          </div>
          <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
            <div class="card-body">
                When 'colorstyle' is set to 'discrete', the number of colors can be selected by this field, DEFAULT=5.
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingSeven">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                <b>'intensestyle'</b>
                TYPE: char | OPTIONS: 'global' , 'local' , 'custom'
              </button>
            </h5>
          </div>
          <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
            <div class="card-body">
                Color intensity is calculated based on the magnitude of YDATA values. When 'global' is selected, intensity will be calculated based on the global minimum and maximum values. When 'local' is selected, intensity is calculated based on each time in the series separately. The maximum and minimum is normalized based on the global maximum and minimum for better color representation. When 'custom' is selected, the set of color intensity given by the users will be used. This is useful for time series plot with different probability. 
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingEight">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                <b>'customintval'</b>
                TYPE: double | OPTIONS: N/A
              </button>
            </h5>
          </div>
          <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
            <div class="card-body">
                It should be initiated when 'intensestyle' is set to 'custom'. It should be the same size as YDATA (N-by-M). Be careful about this option as the element should vary reasonaly by the correspondant values in YDATA.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingNine">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                <b>'midpoint'</b>
                TYPE: char | OPTIONS: 'none' , 'mean' , 'median' , 'custom'
              </button>
            </h5>
          </div>
          <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
            <div class="card-body">
                This option allows user to specify the beginning of the gradient color from certain points to extend to both up and down in Y direction. If 'none', there will start from the lowest levle of YDATA. 'mean' and 'median' automatically find mean and median of the YDATA to start from there. 'custom' allows user to specify a set of points manually. 
            </div>
          </div>
        </div>
          
        <div class="card">
          <div class="card-header" id="headingTen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                <b>'custommidval'</b>
                TYPE: numeric vector | OPTIONS: N/A
              </button>
            </h5>
          </div>
          <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
            <div class="card-body">
                This is mandatory if 'midpoint' is set to 'custom'. It should be vector with the same length as XDATA.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingEleven">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                <b>'jitter'</b>
                TYPE: logical | OPTIONS: true , false
              </button>
            </h5>
          </div>
          <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
            <div class="card-body">
                This is a capability in scatter plot to avoid data opverlapping, and to make a cloud of data. When it is set to on, it will create a distance between cluster of data within a time instance, DEFAULT = false.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingTwelve">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                <b>'jitteramount'</b>
                TYPE: double | OPTIONS: any number between [0,1]
              </button>
            </h5>
          </div>
          <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordion">
            <div class="card-body">
                When 'jitter' is on, it can be set to make distance between clusters, DEFAULT = 0.5.
            </div>
          </div>
        </div>
          
        <div class="card">
          <div class="card-header" id="headingThirteen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                <b>'filledmarker'</b>
                TYPE: logical | OPTIONS: on , off
              </button>
            </h5>
          </div>
          <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordion">
            <div class="card-body">
                When 'linestyle' is set to '.', this switch can be used to fill the market in a scatter plot, DEFAULT = on.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingFourteen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                <b>'markerfacealpha'</b>
                TYPE: double | OPTIONS: any number between [0,1]
              </button>
            </h5>
          </div>
          <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen" data-parent="#accordion">
            <div class="card-body">
                This value is used to tranparent the marker filled color when 'linestyle' is set to '-', DEFAULT = 0.9.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingFifteen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                <b>'markeredgealpha'</b>
                TYPE: double | OPTIONS: any number between [0,1]
              </button>
            </h5>
          </div>
          <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen" data-parent="#accordion">
            <div class="card-body">
                This value is used to tranparent the marker edge color when 'linestyle' is set to '-', DEFAULT = 0.9.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingSixteen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                <b>'showcolorbar'</b>
                TYPE: logical | OPTIONS: on , off
              </button>
            </h5>
          </div>
          <div id="collapseSixteen" class="collapse" aria-labelledby="headingSixteen" data-parent="#accordion">
            <div class="card-body">
                To show the colorbar along the y-axis, DEFAULT = off.
            </div>
          </div>
        </div>
  
        <div class="card">
          <div class="card-header" id="headingSeventeen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
                <b>'colorbarlabel'</b>
                TYPE: char | OPTIONS: any text
              </button>
            </h5>
          </div>
          <div id="collapseSeventeen" class="collapse" aria-labelledby="headingSeventeen" data-parent="#accordion">
            <div class="card-body">
                To assign a label to the colorbar when 'showcolorbar' is set to on, DEFAULT = [].
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingEighteen">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
                <b>'colorbarfontsize'</b>
                TYPE: double | OPTIONS: any number
              </button>
            </h5>
          </div>
          <div id="collapseEighteen" class="collapse" aria-labelledby="headingEighteen" data-parent="#accordion">
            <div class="card-body">
                To set a certain font size for the colorbar label when 'showcolorbar' is set to on, DEFAULT = 12.
            </div>
          </div>
        </div>
    

        </div>
    </div>
    
    <script>
      $('#accordion').collapse();
    </script>
</body>
</html>
